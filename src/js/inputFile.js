const imageContent = document.querySelector('.image-content');

if(window.sessionStorage['image']!= undefined){
  imageContent.style.backgroundImage = 'url('+window.sessionStorage.getItem('image')+')';
}

imageContent.addEventListener('dragover', function(event){
  event.preventDefault();
});

imageContent.addEventListener('drop',function (event) {
  event.preventDefault();
  SetFieldInImage(event.dataTransfer.items[0].getAsFile());
});

function SetFieldInImage(file){
  imageContent.classList.remove('animatedImage');
  var reader = new FileReader();
  reader.onloadend = function () {
    imageContent.style.backgroundImage = 'url('+reader.result+')';
    imageContent.classList.add('animatedImage');
    window.sessionStorage.setItem('image', reader.result);

  }
  reader.readAsDataURL(file);
}

document.querySelector('.image-btn-change').addEventListener('click', function(event){
  var input = document.createElement('input');
  input.type = 'file';
  input.accepet = 'image/*';
  input.click();
  input.addEventListener('change', function(event) {
    SetFieldInImage(event.target.files[0]);
  });
});
