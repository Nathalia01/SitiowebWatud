const typeLabels = {
  'info-level':{
     'origin': 'small',
     'transform': 'input',
   },

   'info-name':{
      'origin': 'h2',
      'transform': 'input',
    },

    'info-description':{
       'origin': 'p',
       'transform': 'textarea',
     },

};
function GenerateEventElement(e){
  e.addEventListener('click', function(event){
  var element = event.target;
  var name = element.className.split(' ')[0];
  var input = document.createElement(typeLabels[name]['transform']);
  input.name= name;
  input.value = element.innerHTML;
  input.className = element.className;
  input.classList.add('input-text');

  element.parentNode.replaceChild(input, element);


//Eventos
input.focus();
    input.addEventListener('keyup',function(event){
        if (event.key == 'Enter'){
            TransformOriginElement(event.target);
        }
    });
    input.addEventListener('blur',function(event){
        TransformOriginElement(event.target);
    });
});
}

function TransformOriginElement(e){
    var name = e.className.split(' ')[0];
    var element = document.createElement(typeLabels[name]['origin']);
    element.className += name +' labelToInput';
    element.innerHTML = e.value;
    GenerateEventElement(element);
     e.parentNode.replaceChild(element, e);
     window.sessionStorage.setItem(name,e.value);
}



var labels = document.querySelectorAll('.labelToInput');
for (var i = labels.length-1; i >=0; i--) {
  var item = labels.item(i);
  var nameItem = item.className.split(' ')[0];
  if(window.sessionStorage[nameItem] != undefined){
    item.innerHTML = window.sessionStorage.getItem(nameItem);
  }
    GenerateEventElement(item);
}
